var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({
    region: 'ap-southeast-1',
    endpoint: "http://localhost:8000",
});

const DynamoDB = require('aws-sdk/clients/dynamodb')

// Create DynamoDB document client
const DocumentClient = new DynamoDB.DocumentClient();


const { Table } = require('dynamodb-toolbox');

// Instantiate a table

const MyTable = new Table({
    // Specify table name (used by DynamoDB)
    name: 'Processed',
    // Define partition and sort keys
    partitionKey: 'PK',
    sortKey: 'SK',
    indexes: {
        "GSI1": { partitionKey: 'Status', sortKey: 'ScheduledDate' },
        "GSI2": { partitionKey: 'TransactionID' }
    },

    // Add the DocumentClient
    DocumentClient
})



const { Entity } = require('dynamodb-toolbox');


const Processed = new Entity({
    // Define attributes
    attributes: {
        PK: { type: 'string', partitionKey: true }, // flag as partitionKey
        SK: { type: 'string', sortKey: true, hidden: true }, // flag as sortKey and mark hidden,
        ScheduledDate: { type: 'string' },
        Status: { type: 'string', default: 'PENDING' },
        TransactionID: { type: 'string', required: true, prefix: 'transaction_' }
    },
    timestamps: false,
    // Assign it to our table
    table: MyTable
})


module.exports.insert_processed = async function (data) {

    return new Promise(async function (resolve, reject) {
        try {
            let item = {
                PK: data.accountID,
                SK: data.SK,
                ScheduledDate: data.ScheduledDates,
                Status: data.Status,
                TransactionID: data.TransactionID
            };

            // Use the 'put' method of Customer
            let result = await Processed.put(item);


            resolve({ status: true, msg: 'เพิิ่มการชำระเงินสำเร็จ' });
        }
        catch (e) {
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
};













