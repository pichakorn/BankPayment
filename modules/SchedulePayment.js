var autoIncrement = require('../modules/autoIncrement');

var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({
    region: 'ap-southeast-1',
    endpoint: "http://localhost:8000",
});

const DynamoDB = require('aws-sdk/clients/dynamodb')

// Create DynamoDB document client
const DocumentClient = new DynamoDB.DocumentClient();


const { Table } = require('dynamodb-toolbox');

// Instantiate a table

const MyTable = new Table({
    // Specify table name (used by DynamoDB)
    name: 'Payments',
    // Define partition and sort keys
    partitionKey: 'PK',
    sortKey: 'SK',
    indexes: {
        "GSI1": { partitionKey: 'Status', sortKey: 'ScheduledDate' },
    },

    // Add the DocumentClient
    DocumentClient
})



const { Entity } = require('dynamodb-toolbox');


const schedulePayment = new Entity({
    // Define attributes
    name: "schedulePayment",
    attributes: {
        PK: { type: 'string', partitionKey: true }, // flag as partitionKey
        SK: { type: 'string', sortKey: true }, // flag as sortKey and mark hidden,
        ScheduledDate: { type: 'string' },
        Status: { type: 'string', default: 'PENDING' },
        TransactionID: { type: 'string', required: false, prefix: 'transaction_' }
    },
    timestamps: false,
    // Assign it to our table
    table: MyTable
})


module.exports.insert_scheduled_payments = async function (data) {

    return new Promise(async function (resolve, reject) {
        try {
            let item = {
                PK: data.accountID,
                SK: data.date,
                ScheduledDate: data.date.split('T')[0],
                Status: 'SCHEDULED',
            }


            // Use the 'put' method of Customer
            let result = await schedulePayment.put(item);





            resolve({ status: true, msg: 'เพิิ่มการชำระเงินสำเร็จ' });
        }
        catch (e) {
            console.log(e);
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
};

module.exports.onProcessing = async function () {

    try {
        // get scheduled to day
        var arr_scheduled = await schedulePayment.query('SCHEDULED', {
            index: 'GSI1',
            eq: new Date().toISOString().split('T')[0]
        });


        // update to processing


        // update to pending waiting for processing
        arr_scheduled.Items.forEach(async (payment) => {
            payment.Status = 'PENDING';
            await schedulePayment.update(payment, {

            });
        });

        // doing processed
        arr_scheduled.Items.forEach(async (payment) => {
            let transactionID = await autoIncrement.autoIncrement('transactionID');
            payment.TransactionID = transactionID.data;
            payment.Status = 'PROCESSED';
            await schedulePayment.update(payment, {

            });
        })


        return;
    }
    catch (e) {
        console.log(e);
    }


}

module.exports.ReturnScheduledPaymentsNext90DayByUserID = async function (userID) {
    return new Promise(async function (resolve, reject) {
        try {

            let start_date = new Date();
            start_date.setDate(start_date.getDate() + 1);
            let last_date = new Date();
            last_date.setDate(last_date.getDate() + 90);
            let arr = await schedulePayment.query(userID, {

                between: [`${start_date.toISOString().split('T')[0]}T17:00:00`, `${last_date.toISOString().split('T')[0]}T17:00:00`]
            })
            resolve({ status: true, data: data.Items })

        }
        catch (e) {
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' })
        }
    })
}

module.exports.ReturnPaymentsByDateAndStatus = function (date, status) {
    return new Promise(async function (resolve, reject) {
        try {
            var arr_scheduled = await schedulePayment.query(status, {
                index: 'GSI1',
                eq: date
            });

            resolve({ status: true, data: arr_scheduled.Items });
        }
        catch (e) {

            console.log(e);
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
}















