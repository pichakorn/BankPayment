
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({
    region: 'ap-southeast-1',
    endpoint: "http://localhost:8000",
});

const DynamoDB = require('aws-sdk/clients/dynamodb')

// Create DynamoDB document client
const DocumentClient = new DynamoDB.DocumentClient();


const { Table } = require('dynamodb-toolbox');

// Instantiate a table

const MyTable = new Table({
    // Specify table name (used by DynamoDB)
    name: 'Autoincrement',
    // Define partition and sort keys
    partitionKey: 'PK',

    // Add the DocumentClient
    DocumentClient
})

const { Entity } = require('dynamodb-toolbox');


const autoIncrement = new Entity({
    // Specify entity name
    name: 'AutoIncrement',
    // Define attributes
    attributes: {
        PK: { type: 'string', partitionKey: true }, // flag as partitionKey
        index: { type: 'number' }
    },
    timestamps: false,
    // Assign it to our table
    table: MyTable
});



module.exports.autoIncrement = async function (collection_name) {

    return new Promise(async function (resolve, reject) {
        try {

            let data = await autoIncrement.get({ PK: collection_name });
            if (!data.Item) {
                // create new
                let item = {
                    PK: collection_name,
                    index: 1
                }

                // Use the 'put' method of Customer
                let result = await autoIncrement.put(item);

                resolve({ status: true, data: 1 });
                return;
            }
            else {
                resolve({ status: true, data: data.Item.index });
                data.Item.index = { $add: 1 };
                await autoIncrement.update(data.Item)
                return;
            }
        }
        catch (e) {
            console.log(e);
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
};