var express = require('express');
var router = express.Router();
const schedule = require('node-schedule');


var Payment = require('../modules/SchedulePayment');

/* GET home page. */

router.post('/insert/payment', async function (req, res) {
    let result = await Payment.insert_scheduled_payments(req.body);
    res.json(result);
});


// every 5 minute process scheduled to day
const job = schedule.scheduleJob('* 5 * * * *', function () {
    Payment.onProcessing()
});






async function main() {

    // (1) Insert scheduled payments.
    await Payment.insert_scheduled_payments({
        "accountID": "peachlaewtae",
        "date": "2021-09-12T17:34:25.778Z"
    });
    await Payment.insert_scheduled_payments({
        "accountID": "peachlaewtae",
        "date": "2021-09-13T17:34:25.778Z"
    });
    await Payment.insert_scheduled_payments({
        "accountID": "peachlaewtae",
        "date": "2021-09-14T17:34:25.778Z"
    });
    await Payment.insert_scheduled_payments({
        "accountID": "peachlaewtae",
        "date": "2021-09-15T17:34:25.778Z"
    });
    await Payment.insert_scheduled_payments({
        "accountID": "peachlaewtae",
        "date": "2021-09-16T17:34:25.778Z"
    });


    // (2) Return scheduled payments by user for the next 90 days.
    await Payment.ReturnScheduledPaymentsNext90DayByUserID('peachlaewtae');


    // (3) Return payments across users for specific date by status 
    var arr_payments = await Payment.ReturnPaymentsByDateAndStatus('2021-09-13', 'SCHEDULED')

}



main();







module.exports = router;
